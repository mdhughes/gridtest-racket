#!/bin/zsh

OSNAME=`uname`
PROGNAME=gridtest

rm -rf build/$PROGNAME.app
mkdir -p build

if [[ $OSNAME == "Darwin" ]]; then
	ICOCMD=--icns
	ICOFILE=$PROGNAME-icon.icns
else
	ICOCMD=--ico
	ICOFILE=$PROGNAME-icon.ico
fi

time raco exe --cs --gui $ICOCMD $ICOFILE -o build/$PROGNAME $PROGNAME.rkt
if [[ $? -ne 0 ]]; then
	exit
fi

if [[ $OSNAME == "Darwin" ]]; then
	cp -R img snd build/$PROGNAME.app/Contents/Resources
fi
